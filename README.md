# Portfolio

### Hello, my name is Florian Kaut

<p>I am currently training as an Applications Developer with CodersBay Vienna. Here, I would like to present you with a selection of projects I have worked on while learning the trade, using languages like Java and Kotlin, as well as frameworks like Vue.js and Jetpack Compose.</p>

# Projects

### Fullstack Project with Springboot and Vue.js
* [Data Management Tool](https://gitlab.com/fullstack-first-semester/data_management_tool_kaut_telli)
<p>This was my project for my first semester, developed in cooperation with my partner Atakan Telli. The goal was to build an administration platform for CodersBay, allowing organization of course participants and grouping them together with assigned coaches and facilities. Here, we combined everything we had learned about development, from the frontend UI/UX using Vue.js, to database integration with MySQL, and the backend logic using Java and Springboot to tie it all together, and created this Administration Tool.
At this early stage in our respective developer careers, the technical aspects were as challenging as they were rewarding. But even more so, working together as a team on a single project taught us a lot about organisation, time management and teamwork.</p>

### Kotlin Jetpack Compose Project
* [Morse Code](https://gitlab.com/coders.bay-web/kaut_ma/-/tree/main/Jetpack_Compose/MorseCode)
<p>In my second semester, I developed this android app in Kotlin, using the Jetpack Compose framework. It allows the user to create custom signal sequences from short and long signals or signal pauses, save the sequence in the database, and finally flash the sequence using the smartphone's flashlight.
Developing this project taught me a lot about component based app design, as well as persistent data storage using Exposed. It also depended on gaining access to the device's hardware.</p>

### Java Project
* [Plastic Slug](https://gitlab.com/codersbay-fia-3/hello-world-2d-game)
<p>For our third semester, my group decided to tackle a new challenge. As a team of nine, we would develop a game, and after some debate, we settled on a 2D sidescrolling shooter inspired by a popular series on the market. 
On the technical side, we are developing in Java, using version 17 of JavaFX's FXGL as our game engine. We are also coordinating our efforts using agile project management, specifically SCRUM, to help facilitate a continuous and productive workflow.
The game, dubbed Plastic Slug, is planned to feature multiple enemies, weapons and pickups, as well as multiplayer integration.</p>

# Languages and Tools

### Web 
<p align="left">
<a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> 
<a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> 
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> 
<a href="https://vuejs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vuejs/vuejs-original-wordmark.svg" alt="vuejs" width="40" height="40"/> </a> 
<a href="https://getbootstrap.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="40" height="40"/> </a> 
<a href="https://www.php.net" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/php/php-original.svg" alt="php" width="40" height="40"/> </a> 
<a href="https://laravel.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/laravel/laravel-plain-wordmark.svg" alt="laravel" width="40" height="40"/> </a> 
</p>


### Java
<p align="left">
<a href="https://www.java.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="40" height="40"/> </a> 
<a href="https://spring.io/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/springio/springio-icon.svg" alt="spring" width="40" height="40"/> </a> 
<a href="https://postman.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="40" height="40"/> </a> 
</p>

### Kotlin
<p align="left">
<a href="https://kotlinlang.org" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/kotlinlang/kotlinlang-icon.svg" alt="kotlin" width="40" height="40"/> </a> 
<a href="https://developer.android.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/android/android-original-wordmark.svg" alt="android" width="40" height="40"/> </a> 
</p>

### Database
<p align="left">
<a href="https://www.mysql.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> </a> 
<a href="https://www.postgresql.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-original-wordmark.svg" alt="postgresql" width="40" height="40"/> </a>
</p>

### Version Control
<p align="left">
<a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a>
</p>

### Design and Prototyping
<p align="left">
<a href="https://www.figma.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="40" height="40"/> </a> 
</p>
